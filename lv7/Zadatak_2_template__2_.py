import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# 1
new_img_arr = np.unique(img_array_aprox, axis=0)
print(f'Number of colors: {len(new_img_arr)}')

# 2
km = KMeans(n_clusters=5, init='random')
km . fit(img_array_aprox)
labels = km . predict(img_array_aprox)

# 3
for i in range(0, len(labels)):
    img_array_aprox[i] = km.cluster_centers_[labels[i]]

# 4
new_img_arr = np.unique(img_array_aprox, axis=0)
print(f'Number of colors in approximated picture: {len(new_img_arr)}')
# povratak na originalnu dimenziju slike
img_aprox = np.reshape(img_array_aprox, (w, h, d))
# povratak iz raspona 0 do 1 u int
img_aprox = (img_aprox*255).astype(np.uint8)
plt.figure()
plt.title("Approximated picture")
plt.imshow(img_aprox)
plt.tight_layout()
plt.show()

# 5 na druge-isto

# 7
un_labels = np.unique(labels)
for i in range(0, len(un_labels)):
    bin_img = labels == un_labels[i]  # labels je n_pixela x 1 shape
    bin_img = np.reshape(bin_img, (w, h))
    plt.figure()
    plt.title(f"Binary image's {i+1} centar")
    plt.imshow(bin_img)
    plt.show()
