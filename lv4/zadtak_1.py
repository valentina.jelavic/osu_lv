from sklearn import datasets
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler, StandardScaler
from sklearn . preprocessing import OneHotEncoder
import sklearn . linear_model as lm

data = pd.read_csv ('data_C02_emission.csv')

data=data.drop(["Make", "Model"], axis=1)

input_variables=[ 'Fuel Consumption City (L/100km)',
                 'Fuel Consumption Hwy (L/100km)',
                 'Fuel Consumption Comb (L/100km)',
                 'Fuel Consumption Comb (mpg)',
                 'Engine Size (L)',
                 'Cylinders'
                 ]

output_variable=['CO2 Emissions (g/km)']
X=data[input_variables].to_numpy()
y=data[output_variable].to_numpy()

#a
X_train , X_test , y_train , y_test = train_test_split (X , y , test_size = 0.2 , random_state =1 )

#b
print(data)
for i in range(0,5):
    plt.scatter(X_train[:,1], y_train, c='blue', s=5)
    plt.scatter(X_test[:,1], y_test, c='red',s=5)
    plt.show()

#c
plt.hist(X_train, bins=20)
plt.show()

sc = StandardScaler()
X_train_n = sc . fit_transform ( X_train )

plt.hist(X_train_n, bins=20)
plt.show()

#d
linearModel = lm.LinearRegression()
linearModel.fit ( X_train_n , y_train )

print(linearModel.LinearRegression())





