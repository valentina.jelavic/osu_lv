import numpy as np
import matplotlib.pyplot as plt

#a
data=np.loadtxt('data.csv', delimiter=',', skiprows=1)
r,c=np.shape(data)
print("data.csv sadrzi", r, "osoba")

#b
h=data[:,1]
w=data[:,2]
plt.scatter(h,w, marker=".")
plt.title("2.b")
plt.show()

#c
he=data[0:10000:50,1]
we=data[0:10000:50,2]
plt.scatter(he,we, marker=".")
plt.title("2.c")
plt.show()

#d
height=data[0:10000:50,1]
print("Minimalna visina u podatkovnom skupu:", height.min())
print("Maksimalna visina u podatkovnom skupu:", height.max())
print("Srednja vrijednost visine u podatkovnom skupu:", height.mean())

#e
Mind = (data[:,0] == 1)

for i in Mind:
    listM=np.array(data[1:10000, 1])


Wind= (data[:,0] == 0)
for i in Wind:
    listW=np.array(data[1:10000, 1])

print("Minimalna visina muskaraca u podatkovnom skupu:", listM.min())
print("Maksimalna visina muskaraca u podatkovnom skupu:", listM.max())
print("Srednja vrijednost visine muskaraca u podatkovnom skupu:", listM.mean())

print("Minimalna visina zena u podatkovnom skupu:", listW.min())
print("Maksimalna visina zena u podatkovnom skupu:", listW.max())
print("Srednja vrijednost  visine zena u podatkovnom skupu:", listW.mean())
