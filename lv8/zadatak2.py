
import numpy as np
from tensorflow import keras
from keras import layers
from keras . models import load_model
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import OneHotEncoder

model = load_model('FCN/')
(X_train, y_train), (X_test, y_test) = keras.datasets.mnist.load_data()
X_test_reshaped = np.reshape(X_test,(len(X_test),X_test.shape[1]*X_test.shape[2])) #za predikciju


# Predikcija na testnom skupu podataka
y_pred = model.predict(X_test_reshaped)
y_pred = np.argmax(y_pred, axis=1)

# Prikazivanje nekoliko losih predikcija
num_rows = 5
num_cols = 5
num_images = num_rows*num_cols
plt.figure(figsize=(2*2*num_cols, 2*num_rows))
for i in range(num_images):
    j = np.random.randint(len(x_test))
    plt.subplot(num_rows, 2*num_cols, 2*i+1)
    plt.imshow(x_test[j], cmap=plt.cm.binary)
    plt.title('Label: {}\nPrediction: {}'.format(y_test[j], np.argmax(y_pred[j])))
    plt.axis('off')
plt.show()