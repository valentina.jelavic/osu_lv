import numpy as np
from tensorflow import keras
from keras import layers
from keras . models import load_model
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import OneHotEncoder

# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()


# 1
print(f'Train: {len(x_train)}')
print(f'Test: {len(y_test)}')
# ulazni podaci imaju oblik (broj primjera,28,28)(svaka slika je 28x28 piksela), svaki piksel predstavljen brojem 0-255
# izlazna velicina kodirana na nacin da su znamenke predstavljene brojevima 0-9
# svaka slika(primjer)-2d matrica, 28x28


# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))


# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)



print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")

y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# 2
# TODO: prikazi nekoliko slika iz train skupa
sample = 2
image = x_train[sample]
fig = plt.figure
plt.imshow(image, cmap='gray')
plt.title('label: {}'.format(y_train[2]))
plt.show()

sample = 5
image = x_train[sample]
fig = plt.figure
plt.imshow(image, cmap='gray')
plt.title('label: {}'.format(y_train[5]))
plt.show()





# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras . Sequential ()
model . add ( layers . Input ( shape =(28,28,1 ) ))
model . add ( layers . Dense (100 , activation ="relu") )
model . add ( layers . Dense (50 , activation ="relu") )
model . add ( layers . Dense (10 , activation ="softmax") )
model . summary ()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model . compile ( loss ="categorical_crossentropy" ,
optimizer ="adam",
metrics = ["accuracy" ,])

x_train = x_train.reshape(-1, 28, 28, 1)
x_test = x_test.reshape(-1, 28, 28, 1)


# TODO: provedi ucenje mreze
batch_size = 10
epochs = 2
history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)
print(history.history)

# TODO: Prikazi test accuracy i matricu zabune
score = model.evaluate( x_test , y_test , verbose=0 )
print("Test accuracy:", score)

y_pred = model.predict( x_test_s )

y_pred_labels = np.argmax(y_pred, axis=1)

# Get the true class labels for the test data
y_true_labels = np.argmax(y_test_s, axis=1)

# Compute the confusion matrix
cm = confusion_matrix(y_true_labels, y_pred_labels)

# Display the confusion matrix
print('Confusion matrix:\n', cm)


# TODO: spremi model
model.save ("FCN/")
del model
model = load_model ('FCN/')
model.summary()