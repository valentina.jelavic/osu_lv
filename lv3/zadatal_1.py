import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd . read_csv('data_C02_emission.csv')

# a
print(len(data))
print(data.dtypes)

result = len(data.drop_duplicates())
if result != len(data):
    print("Sadrzi duplikate")
    data = data.drop_duplicates()
else:
    print("Ne postoje duplikati")


if data.isnull().sum().sum() == 0:
    print("Nema")
else:
    print("Ima")
    data.dropna()

for col in ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']:
    data[col] = data[col].astype('category')

print(data.dtypes)

#b
newD = data.sort_values(by='Fuel Consumption City (L/100km)', ascending=True)
print(newD['Fuel Consumption City (L/100km)'])
print(newD.iloc[0:3, [0,1,7]])

newDD = data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False)
print(newDD['Fuel Consumption City (L/100km)'])
print(newDD.iloc[0:3, [0,1,7]])

#c
dataC=data[(data['Engine Size (L)']> 2.5) & (data['Engine Size (L)']< 3.5)]
print(len(dataC))


print(dataC['CO2 Emissions (g/km)'].mean())

#d
