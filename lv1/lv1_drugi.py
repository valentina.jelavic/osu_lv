try:
    print("Unesite broj:")
    number=float(input())
    
except Exception:
    print("Moras nesto upisati")
    
number=float(input())
while number<0.0 or number>1.0:
    print("Broj treba biti u rasponu 0.0 do 1.0, unesite ponovno:")
    number=float(input())

if number>=0.9:
    print("A")
elif number>=0.8:
    print("B")
elif number>=0.7:
    print("C")
elif number>=0.6:
    print("D")
else:
    print("F")
